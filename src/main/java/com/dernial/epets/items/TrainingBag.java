package com.dernial.epets.items;

import com.dernial.epets.ExpandedPets;
import com.dernial.epets.entitys.EpetsWolfEntity;
import com.dernial.epets.entitys.ModEntities;
import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.block.BlockState;
import net.minecraft.block.IBucketPickupHandler;
import net.minecraft.block.ILiquidContainer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.passive.WolfEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.*;
import net.minecraft.util.math.*;
import net.minecraft.world.World;

import java.util.Objects;

public class TrainingBag extends Item
{
    public TrainingBag()
    {
        super(new Item.Properties()
                .maxStackSize(1)
                .group(ExpandedPets.setup.itemGroup)
        );
        setRegistryName(ExpandedPets.MOD_ID, "training_bag");
    }

    @Override
    public boolean itemInteractionForEntity(ItemStack item, PlayerEntity player, LivingEntity entity, Hand hand)
    {
        System.out.println(entity.getName());

        if(entity instanceof WolfEntity)
        {
            WolfEntity old = (WolfEntity) entity;

            if(old.isTamed())
            {
                if(old.isOwner(player))
                {
                    BlockPos pos = entity.getPosition();
                    World world = entity.getEntityWorld();

                    Entity res = ModEntities.Wolf.spawn(world, item, player, pos, SpawnReason.SPAWN_EGG, true, true);

                    if (res != null)
                    {
                        Item i = item.getItem();

                        if (i instanceof TrainingBag)
                        {
                            ((TrainingBag) i).ReduceUses(1);
                        }

                        EpetsWolfEntity newWolf = (EpetsWolfEntity) res;
                        newWolf.copyDataFromOld(old);
                        old.remove();

                        return true;
                    }
                }
            }
        }

        return super.itemInteractionForEntity(item, player, entity, hand);
    }

    public void ReduceUses(int count)
    {

    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        if(isSelected)
        {
            if(entityIn instanceof PlayerEntity)
            {
            }
        }
    }
}

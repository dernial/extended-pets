package com.dernial.epets.items;

import com.dernial.epets.ExpandedPets;
import net.minecraft.item.Item;

public class PetFood extends Item {
    public PetFood() {
        super(new Item.Properties()
                .maxStackSize(64)
                .group(ExpandedPets.setup.itemGroup)
        );
        setRegistryName(ExpandedPets.MOD_ID,"pet_food");
    }
}

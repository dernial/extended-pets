package com.dernial.epets.items;

import net.minecraft.item.Item;
import net.minecraftforge.registries.ObjectHolder;

public class ModItems {

    @ObjectHolder("epets:training_bag")
    public static TrainingBag TrainingBag;

    @ObjectHolder("epets:pet_food")
    public static PetFood PetFood;
}

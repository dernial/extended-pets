package com.dernial.epets.blocks;

import net.minecraft.inventory.container.ContainerType;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.registries.ObjectHolder;

public class ModBlocks {

    @ObjectHolder("epets:kennel")
    public static Kennel Kennel;

    @ObjectHolder("epets:kennel")
    public static TileEntityType<KennelTile> KennelTile;

    @ObjectHolder("epets:kennel")
    public static ContainerType<KennelContainer> KennelContainer;
}

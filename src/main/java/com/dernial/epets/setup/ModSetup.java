package com.dernial.epets.setup;

import com.dernial.epets.blocks.ModBlocks;
import net.minecraft.block.Block;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.Tag;
import net.minecraft.util.ResourceLocation;

public class ModSetup {

    public static final Tag<Block> KENNELS = makeWrapperTag("kennels");

    public ItemGroup itemGroup = new ItemGroup("epets") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(ModBlocks.Kennel);
        }
    };

    private static Tag<Block> makeWrapperTag(String id) {
        return new BlockTags.Wrapper(new ResourceLocation("epets", id));
    }
}

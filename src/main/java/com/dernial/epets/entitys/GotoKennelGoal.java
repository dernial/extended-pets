package com.dernial.epets.entitys;

import com.dernial.epets.blocks.Kennel;
import com.dernial.epets.blocks.KennelTile;
import com.dernial.epets.setup.ModSetup;
import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.goal.MoveToBlockGoal;
import net.minecraft.entity.passive.CatEntity;
import net.minecraft.tags.BlockTags;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;

import java.util.EnumSet;

public class GotoKennelGoal extends MoveToBlockGoal
{
    private final EpetsWolfEntity dog;
    private final double allowedDistanceFromPlayer;

    public GotoKennelGoal(EpetsWolfEntity dog, double speed, int searchLength, double distanceFromPlayer) {
        super(dog, speed, searchLength, 6);
        this.dog = dog;
        this.field_203112_e = -2;
        this.allowedDistanceFromPlayer = distanceFromPlayer;
        this.setMutexFlags(EnumSet.of(Goal.Flag.JUMP, Goal.Flag.MOVE));
    }

    public boolean shouldExecute() {
        return this.dog.isTamed() && !this.dog.isSitting() && !this.dog.isSittingInKennel() && super.shouldExecute();
    }

    public boolean shouldContinueExecuting() {
        return this.dog.getPosition().withinDistance(this.dog.getOwner().getPositionVec(),  this.allowedDistanceFromPlayer);
    }

    public void startExecuting() {
        super.startExecuting();
        this.dog.getAISit().setSitting(false);
    }

    protected int getRunDelay(CreatureEntity p_203109_1_) {
        return 5;
    }

    public void resetTask() {
        super.resetTask();
        this.dog.setSittingInKennel(false, null);
    }

    public void tick() {
        super.tick();

        this.dog.getAISit().setSitting(false);

        if (!this.getIsAboveDestination()) {
            TileEntity te = this.dog.getEntityWorld().getTileEntity(this.destinationBlock);
            this.dog.setSittingInKennel(false, te);
        } else if (!this.dog.isSittingInKennel()) {
            TileEntity te = this.dog.getEntityWorld().getTileEntity(this.destinationBlock);
            this.dog.setSittingInKennel(true, te);
        }

    }

    protected boolean shouldMoveTo(IWorldReader p_179488_1_, BlockPos p_179488_2_) {
        return p_179488_1_.isAirBlock(p_179488_2_.up()) && p_179488_1_.getBlockState(p_179488_2_).getBlock().isIn(ModSetup.KENNELS);
    }
}

package com.dernial.epets.entitys;

import net.minecraft.entity.EntityType;
import net.minecraftforge.registries.ObjectHolder;

public class ModEntities
{
    @ObjectHolder("epets:wolf")
    public static EntityType<EpetsWolfEntity> Wolf;
}

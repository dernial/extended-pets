package com.dernial.epets.entitys;

import com.dernial.epets.abilities.DamagePetAbility;
import com.dernial.epets.abilities.PetAbility;
import com.dernial.epets.blocks.KennelTile;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.monster.AbstractSkeletonEntity;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.entity.passive.TurtleEntity;
import net.minecraft.entity.passive.WolfEntity;
import net.minecraft.entity.passive.horse.LlamaEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;

import java.util.ArrayList;
import java.util.List;

public class EpetsWolfEntity extends WolfEntity
{
    private static final DataParameter<Boolean> sittingInKennelStatus;
    private KennelTile myKennel;
    private List<PetAbility> myAbilities = new ArrayList<PetAbility>();

    public EpetsWolfEntity(EntityType<? extends WolfEntity> entityType, World world)
    {
        super(entityType, world);

        myAbilities.add(new DamagePetAbility());
    }

    public List<PetAbility> getMyAbilities()
    {
        return myAbilities;
    }

    @Override
    protected void registerGoals() {
        super.registerGoals();

        this.goalSelector.addGoal(6, new GotoKennelGoal(this, 1.1D, 8, 10));
    }

    @Override
    protected void registerData() {
        super.registerData();
        this.dataManager.register(sittingInKennelStatus, false);
    }

    public void setSittingInKennel(boolean value, TileEntity te)
    {
        this.dataManager.set(sittingInKennelStatus, value);

        if(value)
        {
            if(te != null && te instanceof KennelTile)
            {
                ((KennelTile)te).SetDog(this);
                myKennel = (KennelTile)te;
            }
        }
        else
        {
            if(this.myKennel != null)
            {
                this.myKennel.SetDog(null);
                this.myKennel = null;
            }
        }
    }

    public boolean isSittingInKennel() {
        return (Boolean)this.dataManager.get(sittingInKennelStatus);
    }

    static {
        sittingInKennelStatus = EntityDataManager.createKey(EpetsWolfEntity.class, DataSerializers.BOOLEAN);
    }
}

package com.dernial.epets.abilities;

import com.dernial.epets.entitys.EpetsWolfEntity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;

public class DamagePetAbility extends PetAbility
{
    protected AttributeModifier myModifier;

    public DamagePetAbility()
    {
        this.valueTillIncrement = 100;
    }

    @Override
    public String getName()
    {
        return "Damage";
    }

    @Override
    public EpetsWolfEntity UpdateEntity(EpetsWolfEntity e)
    {
        if(this.myModifier != null)
        {
            e.getAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).removeModifier(this.myModifier);
        }

        this.myModifier = new AttributeModifier("TrainedDamage", this.levels, AttributeModifier.Operation.ADDITION);
        e.getAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).applyModifier(this.myModifier);

        return e;
    }
}

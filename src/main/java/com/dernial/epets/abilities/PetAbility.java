package com.dernial.epets.abilities;

import com.dernial.epets.entitys.EpetsWolfEntity;

public abstract class PetAbility
{
    protected int value;
    protected int valueTillIncrement;
    protected int levels;

    public abstract String getName();
    public abstract EpetsWolfEntity UpdateEntity(EpetsWolfEntity e);

    public int getValue()
    {
        return this.value;
    }

    public int getLevel()
    {
        return levels;
    }

    protected boolean checkIncrementLevel()
    {
        if (this.value >= this.valueTillIncrement)
        {
            this.value -= this.valueTillIncrement;
            this.levels++;
            return true;
        }

        return false;
    }

    public boolean incrementValue()
    {
        this.value += 10;
        return this.checkIncrementLevel();
    }
}

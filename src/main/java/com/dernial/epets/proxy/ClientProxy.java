package com.dernial.epets.proxy;

import com.dernial.epets.blocks.ModBlocks;
import com.dernial.epets.screens.KennelScreen;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

public class ClientProxy extends CommonProxy
{
    /**
     * Run before anything else. Read your config, create blocks, items, etc, and register them with the GameRegistry
     */
    @Override
    public void preInit()
    {
        super.preInit();

    }

    /**
     * Do your mod setup. Build whatever data structures you care about. Register recipes,
     * send FMLInterModComms messages to other mods.
     */
    @Override
    public void init()
    {
        super.init();

        // Guis
        ScreenManager.registerFactory(ModBlocks.KennelContainer, KennelScreen::new);
    }

    /**
     * Handle interaction with other mods, complete your setup based on this.
     */
    @Override
    public void postInit()
    {
        super.postInit();

    }

    @Override
    public boolean isDedicatedServer() {return false;}

    @Override
    public World getClientWorld() {
        return Minecraft.getInstance().world;
    }

    @Override
    public PlayerEntity getClientPlayer() {
        return Minecraft.getInstance().player;
    }
}

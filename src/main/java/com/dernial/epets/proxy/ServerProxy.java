package com.dernial.epets.proxy;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

public class ServerProxy extends CommonProxy
{
    /**
     * Run before anything else. Read your config, create blocks, items, etc, and register them with the GameRegistry
     */
    @Override
    public void preInit()
    {
        super.preInit();

    }

    /**
     * Do your mod setup. Build whatever data structures you care about. Register recipes,
     * send FMLInterModComms messages to other mods.
     */
    @Override
    public void init()
    {
        super.init();

    }

    /**
     * Handle interaction with other mods, complete your setup based on this.
     */
    @Override
    public void postInit()
    {
        super.postInit();

    }

    @Override
    public boolean isDedicatedServer() {return true;}

    @Override
    public World getClientWorld() {
        throw new IllegalStateException("Only run this on the client!");
    }

    @Override
    public PlayerEntity getClientPlayer() {
        throw new IllegalStateException("Only run this on the client!");
    }
}

package com.dernial.epets.screens;

import com.dernial.epets.ExpandedPets;
import com.dernial.epets.abilities.PetAbility;
import com.dernial.epets.blocks.KennelContainer;
import com.dernial.epets.blocks.KennelTile;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

public class KennelScreen extends ContainerScreen<KennelContainer> {

    private ResourceLocation GUI = new ResourceLocation(ExpandedPets.MOD_ID, "textures/gui/kennel_gui.png");
    private KennelTile TE;

    public KennelScreen(KennelContainer container, PlayerInventory inventory, ITextComponent text) {
        super(container, inventory, text);

        if(this.container.getTileEntity() instanceof KennelTile)
        {
            this.TE = (KennelTile)this.container.getTileEntity();
        }
    }

    @Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        this.renderBackground();
        super.render(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        //drawString(Minecraft.getInstance().fontRenderer, "Food: " + ((KennelTile)this.container.getTileEntity()).getMyCustomData(), 10, 10, 0xffffff);

        if(this.TE != null)
        {
            int currentY = 15;
            for(PetAbility a : this.TE.getPetAbilities())
            {
                drawString(Minecraft.getInstance().fontRenderer, a.getName() + " : " + a.getLevel(), 30, currentY, 0xffffff);
                currentY += 10;
            }
        }
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        this.minecraft.getTextureManager().bindTexture(GUI);
        int relX = (this.width - this.xSize) / 2;
        int relY = (this.height - this.ySize) / 2;
        this.blit(relX, relY, 0, 0, this.xSize, this.ySize);
    }
}
